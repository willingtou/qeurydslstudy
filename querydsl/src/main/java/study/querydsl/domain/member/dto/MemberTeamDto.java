package study.querydsl.domain.member.dto;

import com.querydsl.core.annotations.QueryProjection;
import lombok.Data;

@Data
public class MemberTeamDto {

    private Long memberId;
    private String username;
    private int tage;
    private Long teamId;
    private String teamName;

    @QueryProjection
    public MemberTeamDto(Long memberId, String username, int tage, Long teamId, String teamName) {
        this.memberId = memberId;
        this.username = username;
        this.tage = tage;
        this.teamId = teamId;
        this.teamName = teamName;
    }
}
